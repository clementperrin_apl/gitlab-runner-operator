package runner

import (
	"testing"

	"gotest.tools/assert"
	is "gotest.tools/assert/cmp"

	gitlabv1beta2 "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/api/v1beta2"
	corev1 "k8s.io/api/core/v1"
)

func TestGetEnvironmentVariables(t *testing.T) {
	tests := map[string]struct {
		spec           gitlabv1beta2.RunnerSpec
		expectedEnvs   []corev1.EnvVar
		unexpectedEnvs []string
	}{
		"custom helper image": {
			spec: gitlabv1beta2.RunnerSpec{HelperImage: "custom-helper-image"},
			expectedEnvs: []corev1.EnvVar{
				{
					Name:  envVarKubernetesHelperImage,
					Value: "custom-helper-image",
				},
			},
		},
		"locked default false": {
			spec: gitlabv1beta2.RunnerSpec{},
			expectedEnvs: []corev1.EnvVar{
				{
					Name:  "REGISTER_LOCKED",
					Value: "false",
				},
			},
		},
		"locked set false": {
			spec: gitlabv1beta2.RunnerSpec{Locked: false},
			expectedEnvs: []corev1.EnvVar{
				{
					Name:  "REGISTER_LOCKED",
					Value: "false",
				},
			},
		},
		"locked set true": {
			spec: gitlabv1beta2.RunnerSpec{Locked: true},
			expectedEnvs: []corev1.EnvVar{
				{
					Name:  "REGISTER_LOCKED",
					Value: "true",
				},
			},
		},
		"run untagged unspecified": {
			spec: gitlabv1beta2.RunnerSpec{},
			unexpectedEnvs: []string{
				"REGISTER_RUN_UNTAGGED",
			},
		},
		"run untagged set false": {
			spec: gitlabv1beta2.RunnerSpec{RunUntagged: func(b bool) *bool { return &b }(false)},
			expectedEnvs: []corev1.EnvVar{
				{
					Name:  "REGISTER_RUN_UNTAGGED",
					Value: "false",
				},
			},
		},
		"run untagged set true": {
			spec: gitlabv1beta2.RunnerSpec{RunUntagged: func(b bool) *bool { return &b }(true)},
			expectedEnvs: []corev1.EnvVar{
				{
					Name:  "REGISTER_RUN_UNTAGGED",
					Value: "true",
				},
			},
		},
		"protected default false": {
			spec: gitlabv1beta2.RunnerSpec{},
			unexpectedEnvs: []string{
				"REGISTER_ACCESS_LEVEL",
			},
		},
		"protected set false": {
			spec: gitlabv1beta2.RunnerSpec{Protected: false},
			unexpectedEnvs: []string{
				"REGISTER_ACCESS_LEVEL",
			},
		},
		"protected set true": {
			spec: gitlabv1beta2.RunnerSpec{Protected: true},
			expectedEnvs: []corev1.EnvVar{
				{
					Name:  "REGISTER_ACCESS_LEVEL",
					Value: "ref_protected",
				},
			},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			ctrl := New(getTestRunner())
			ctrl.cr.Spec = tt.spec

			imageResolverMock := &mockImageResolver{}
			imageResolverMock.On("HelperImage").Return("default")
			ctrl.imageResolver = imageResolverMock

			vars := ctrl.getEnvironmentVariables(nil)
			for _, tv := range tt.expectedEnvs {
				assert.Assert(t, is.Contains(vars, tv))
			}

			for _, v := range vars {
				for _, tv := range tt.unexpectedEnvs {
					assert.Assert(t, v.Name != tv, "%v contains %v", vars, tv)
				}
			}
		})
	}
}
