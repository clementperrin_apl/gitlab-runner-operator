# Current Operator version
VERSION ?= v0.0.1
# Even if an empty VERSION is passed we override it
# since we always need VERSION. This is useful for dev builds
# of the operator and bundle images
ifeq ($(VERSION),)
override VERSION := v0.0.1
endif

# The Revision is used for unofficial builds since we need a semver-compatible
# version for the bundle
ifneq ($(REVISION),)
override VERSION := $(VERSION)-$(REVISION)
endif

# Current GitLab Runner version
APP_VERSION ?= v$(shell cat APP_VERSION)

# Toggles between building certified images for RedHat's registry
# or building for the GitLab registry
CERTIFIED ?= false

GITLAB_RUNNER_OPERATOR_REGISTRY ?= registry.gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator

# Default bundle image tag
BUNDLE_IMG ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/gitlab-runner-operator-bundle:$(VERSION)
GITLAB_CHANGELOG_VERSION ?= master
GITLAB_CHANGELOG = .tmp/gitlab-changelog-$(GITLAB_CHANGELOG_VERSION)
# Options for 'bundle-build'
BUNDLE_CHANNELS ?= stable
BUNDLE_DEFAULT_CHANNEL ?= stable
BUNDLE_METADATA_OPTS ?= --channels=$(BUNDLE_CHANNELS) --default-channel=$(BUNDLE_DEFAULT_CHANNEL)

# Image URL to use all building/pushing image targets
IMG ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/gitlab-runner-operator:$(VERSION)
# Produce CRDs that work back to Kubernetes 1.11 (no version conversion)
CRD_OPTIONS ?= crd:trivialVersions=true,preserveUnknownFields=false

CSV_FILE = gitlab-runner-operator.clusterserviceversion.yaml

SHELL=/bin/bash

PLATFORMS ?= linux/amd64
PLATFORM ?= linux/amd64
ARCH ?= amd64
OS ?= linux

BINDIR = $(shell pwd)/.tmp

MACHINE_NAME ?= $(shell uname -m)
HOST_ARCH ?= $(MACHINE_NAME)
ifeq ($(HOST_ARCH),x86_64)
override HOST_ARCH := amd64
endif

HOST_OS ?= $(shell uname | tr '[:upper:]' '[:lower:]')

PIP ?= python3 -m pip

GITLAB_CHANGELOG = $(BINDIR)/gitlab-changelog
$(GITLAB_CHANGELOG): VERSION = master
$(GITLAB_CHANGELOG): DOWNLOAD_URL = https://storage.googleapis.com/gitlab-runner-tools/gitlab-changelog/$(VERSION)/gitlab-changelog-$(HOST_OS)-$(HOST_ARCH)
$(GITLAB_CHANGELOG): DIR = $(shell dirname $(GITLAB_CHANGELOG))
$(GITLAB_CHANGELOG):
	# Installing $(DOWNLOAD_URL) as $(GITLAB_CHANGELOG)
	@mkdir -p $(DIR)
	@curl -sL $(DOWNLOAD_URL) -o $(GITLAB_CHANGELOG)
	@chmod +x $(GITLAB_CHANGELOG)

MOCKERY ?= $(BINDIR)/mockery
$(MOCKERY): VERSION = 1.1.0
$(MOCKERY): DOWNLOAD_URL = https://github.com/vektra/mockery/releases/download/v$(VERSION)/mockery_$(VERSION)_$(HOST_OS)_$(MACHINE_NAME).tar.gz
$(MOCKERY): DIR = $(shell dirname $(MOCKERY))
$(MOCKERY):
	# Installing $(DOWNLOAD_URL) as $(MOCKERY)
	@mkdir -p $(DIR)
	@curl -sL "$(DOWNLOAD_URL)" | tar xz -O mockery > $(MOCKERY)
	@chmod +x $(MOCKERY)

OPERATOR_SDK ?= $(BINDIR)/operator-sdk
$(OPERATOR_SDK): VERSION = v1.12.0
$(OPERATOR_SDK): DOWNLOAD_URL = https://github.com/operator-framework/operator-sdk/releases/download/$(VERSION)/operator-sdk_$(HOST_OS)_$(HOST_ARCH)
$(OPERATOR_SDK): DIR = $(shell dirname $(OPERATOR_SDK))
$(OPERATOR_SDK):
	# Installing $(DOWNLOAD_URL) as $(OPERATOR_SDK)
	@mkdir -p $(DIR)
	@curl -Lo $(OPERATOR_SDK) $(DOWNLOAD_URL)
	@chmod +x $(OPERATOR_SDK)

YQ ?= $(BINDIR)/yq
$(YQ): VERSION = v4.4.1
$(YQ): DOWNLOAD_URL = https://github.com/mikefarah/yq/releases/download/$(VERSION)/yq_$(HOST_OS)_$(HOST_ARCH)
$(YQ): DIR = $(shell dirname $(YQ))
$(YQ):
	# Installing $(DOWNLOAD_URL) as $(YQ)
	@mkdir -p $(DIR)
	@curl -Lo $(YQ) $(DOWNLOAD_URL)
	@chmod +x $(YQ)

OPM_HOST_OS := $(HOST_OS)
ifeq ($(HOST_OS),darwin)
override OPM_HOST_OS := mac
endif

OPM ?= $(BINDIR)/opm
$(OPM): VERSION = 4.6.22
$(OPM): ARCHIVE_NAME = opm-$(OPM_HOST_OS)-$(VERSION).tar.gz
$(OPM): DOWNLOAD_URL = https://mirror.openshift.com/pub/openshift-v4/$(MACHINE_NAME)/clients/ocp/$(VERSION)/$(ARCHIVE_NAME)
$(OPM): DIR = $(shell dirname $(OPM))
$(OPM):
	# Installing $(DOWNLOAD_URL) as $(OPM)
	@mkdir -p $(DIR)
	@curl -Lo $(DIR)/$(ARCHIVE_NAME) $(DOWNLOAD_URL)
	@tar -C $(DIR) -zxvf $(DIR)/$(ARCHIVE_NAME)
	@# The darwin archive contains a platform specific binary while the linux archive does not
	@mv $(DIR)/$(HOST_OS)-$(HOST_ARCH)-opm $(OPM) || true
	@chmod +x $(OPM)

KUSTOMIZE ?= $(BINDIR)/kustomize
$(KUSTOMIZE): VERSION = v3.8.8
$(KUSTOMIZE): ARCHIVE_NAME = kustomize_$(VERSION)_$(HOST_OS)_$(HOST_ARCH).tar.gz
$(KUSTOMIZE): DOWNLOAD_URL = https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F$(VERSION)/$(ARCHIVE_NAME)
$(KUSTOMIZE): DIR = $(shell dirname $(KUSTOMIZE))
$(KUSTOMIZE):
	# Installing $(DOWNLOAD_URL) as $(KUSTOMIZE)
	@mkdir -p $(DIR)
	@curl -Lo $(DIR)/$(ARCHIVE_NAME) $(DOWNLOAD_URL)
	@tar -C $(DIR) -zxvf $(DIR)/$(ARCHIVE_NAME)
	@chmod +x $(KUSTOMIZE)

_install_go_binary: BINARY_VERSION ?=
_install_go_binary: DOWNLOAD_URL ?=
_install_go_binary: export GOBIN = $(BINDIR)
_install_go_binary:
	go install $(DOWNLOAD_URL)@$(BINARY_VERSION)

CONTROLLER_GEN ?= $(BINDIR)/controller-gen
$(CONTROLLER_GEN):
	$(MAKE) _install_go_binary BINARY_VERSION=v0.6.1 DOWNLOAD_URL=sigs.k8s.io/controller-tools/cmd/controller-gen

P2 ?= $(BINDIR)/p2
$(P2):
	$(MAKE) _install_go_binary BINARY_VERSION=r12 DOWNLOAD_URL=github.com/wrouesnel/p2cli/cmd/p2

GOLINT ?= $(BINDIR)/golint
$(GOLINT):
	$(MAKE) _install_go_binary BINARY_VERSION=master DOWNLOAD_URL=golang.org/x/lint/golint

SKOPEO ?= $(shell which skopeo)
$(SKOPEO):
	@echo "WARNING:"
	@echo "WARNING: Skopeo requires manual installation. Please see https://github.com/containers/skopeo/blob/main/install.md"
	@echo "WARNING:"

tools:
	$(MAKE) $(GITLAB_CHANGELOG) $(MOCKERY) $(OPERATOR_SDK) $(YQ) $(OPM) $(KUSTOMIZE) $(CONTROLLER_GEN) $(SKOPEO)

all: manager

# Run tests
test:
	go test -v ./...

lint: $(GOLINT)
lint:
	$(GOLINT) -set_exit_status $(shell go list ./... | grep -v /vendor/)

# Build manager binary
manager: generate fmt vet
	go build -o bin/manager main.go

# Run against the configured Kubernetes cluster in ~/.kube/config
run: generate fmt vet manifests
	go run ./main.go

# Install CRDs into a cluster
install: manifests $(KUSTOMIZE)
	$(KUSTOMIZE) build config/crd | kubectl apply -f -

# Uninstall CRDs from a cluster
uninstall: manifests $(KUSTOMIZE)
	$(KUSTOMIZE) build config/crd | kubectl delete -f -

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy: manifests $(KUSTOMIZE)
	cd config/manager && $(KUSTOMIZE) edit set image controller=$(IMG)
	$(KUSTOMIZE) build config/default | kubectl apply -f -

undeploy: manifests $(KUSTOMIZE)
	$(KUSTOMIZE) build config/default | kubectl delete -f -

# Generate manifests e.g. CRD, RBAC etc.
manifests: $(CONTROLLER_GEN)
manifests: CRDS_DIR ?= config/crd/bases
manifests:
	$(CONTROLLER_GEN) $(CRD_OPTIONS) rbac:roleName=manager-role webhook paths="./..." output:crd:artifacts:config=$(CRDS_DIR)

clean-bundle:
	rm -rf bundle/manifests

create-kustomization: $(KUSTOMIZE)
	@./scripts/create_kustomization.sh

render-template: $(P2)
render-template: TEMPLATE ?=
render-template: TO_DIR ?=
render-template: VARS ?=
render-template:
	mkdir -p $(TO_DIR)
	echo $(VARS) > $(BINDIR)/$(TEMPLATE)-vars.env
	# For some reason p2 doesn't work with variables from stdin
	$(P2) -t config/templates/$(TEMPLATE) -i $(BINDIR)/$(TEMPLATE)-vars.env > $(TO_DIR)/$(TEMPLATE)

render-csv-template: MANIFESTS_DIR ?=
render-csv-template:
	$(MAKE) render-template TEMPLATE=$(CSV_FILE) TO_DIR=$(MANIFESTS_DIR)/bases VARS="GITLAB_RUNNER_VERSION=$(shell cat APP_VERSION)"

# Generate bundle manifests and metadata, then validate generated files.
.PHONY: bundle
bundle: export MANIFESTS_DIR = config/manifests
bundle: $(KUSTOMIZE) $(OPERATOR_SDK)
bundle: clean-bundle manifests create-kustomization
bundle:
	$(MAKE) render-csv-template

	$(OPERATOR_SDK) generate kustomize manifests -q
	cd config/manager && $(KUSTOMIZE) edit set image controller=$(IMG)
	$(KUSTOMIZE) build $(MANIFESTS_DIR) | $(OPERATOR_SDK) generate bundle -q --overwrite --version $(subst v,,$(VERSION)) $(BUNDLE_METADATA_OPTS)
	$(OPERATOR_SDK) bundle validate ./bundle

# Install CRDs into a cluster
install-k8s: manifests-k8s $(KUSTOMIZE)
	$(KUSTOMIZE) build config/crd_k8s | kubectl apply -f -

# Uninstall CRDs from a cluster
uninstall-k8s: manifests-k8s $(KUSTOMIZE)
	$(KUSTOMIZE) build config/crd_k8s | kubectl delete -f -

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy-k8s: manifests-k8s $(KUSTOMIZE)
	cd config/manager && $(KUSTOMIZE) edit set image controller=$(IMG)
	$(KUSTOMIZE) build config/default_k8s | kubectl apply -f -

undeploy-k8s: manifests-k8s $(KUSTOMIZE)
	$(KUSTOMIZE) build config/default_k8s | kubectl delete -f -

manifests-k8s:
	$(MAKE) manifests CRDS_DIR=config/crd_k8s/bases

# Generate bundle manifests and metadata, then validate generated files.
bundle-k8s: export MANIFESTS_DIR = config/manifests_k8s
bundle-k8s: $(KUSTOMIZE) $(OPERATOR_SDK) $(YQ)
bundle-k8s: clean-bundle manifests-k8s create-kustomization
bundle-k8s:
	$(MAKE) render-csv-template

	$(OPERATOR_SDK) generate kustomize manifests -q --input-dir=$(MANIFESTS_DIR) --output-dir=$(MANIFESTS_DIR)
	$(YQ) e '.spec.installModes[0].supported = false' -i $(MANIFESTS_DIR)/bases/$(CSV_FILE)
	cd config/manager && $(KUSTOMIZE) edit set image controller=$(IMG)
	$(KUSTOMIZE) build $(MANIFESTS_DIR) --output=$(MANIFESTS_DIR)/resources.yaml
	$(OPERATOR_SDK) generate bundle -q --overwrite --version $(subst v,,$(VERSION)) --input-dir $(MANIFESTS_DIR) $(BUNDLE_METADATA_OPTS)
	$(OPERATOR_SDK) bundle validate ./bundle

# Run go fmt against code
fmt:
	go fmt ./...

# Run go vet against code
vet:
	go vet ./...

# Generate code
generate: $(CONTROLLER_GEN)
	$(CONTROLLER_GEN) object:headerFile="hack/boilerplate.go.txt" paths="./..."

# Build the docker image
docker-build:
	docker buildx build --platform "${PLATFORM}" -t gitlab-runner-operator:${ARCH}-${VERSION} --output type=image --build-arg ARCH=${ARCH} --build-arg OS=${OS} --build-arg VERSION=${VERSION} .

docker-tag:
	docker tag gitlab-runner-operator:${ARCH}-${VERSION} ${PROJECT}/gitlab-runner-operator:${ARCH}-${VERSION}

# Push the docker image
docker-push:
	docker push ${PROJECT}/gitlab-runner-operator:${ARCH}-${VERSION}

# Build the bundle image.
.PHONY: bundle-build
bundle-build:
	docker build -f bundle.Dockerfile -t $(BUNDLE_IMG) .

install-pip-dependencies:
	$(PIP) install -r hack/scripts/requirements.txt
	$(PIP) install -r scripts/ci/requirements.txt

certification-bundle: install-pip-dependencies $(KUSTOMIZE)
	$(KUSTOMIZE) build bundle | scripts/ci/parse_bundle.py

certification-bundle-build:
	docker buildx build --platform=${PLATFORM} -f certification.Dockerfile -t gitlab-runner-operator-bundle:${VERSION} .

certification-bundle-tag:
	docker tag gitlab-runner-operator-bundle:${VERSION} ${PROJECT}/gitlab-runner-operator-bundle:${VERSION}

certification-bundle-push:
	docker push ${PROJECT}/gitlab-runner-operator-bundle:${VERSION}

manifest-operator-image: install-pip-dependencies
	CERTIFIED=${CERTIFIED} PLATFORMS=${PLATFORMS} ./scripts/ci/build.py manifest operator --push ${VERSION} ${APP_VERSION}

build-and-push-operator-image: install-pip-dependencies
	CERTIFIED=${CERTIFIED} ./scripts/ci/build.py operator-image --push ${VERSION} ${APP_VERSION}

build-and-push-bundle-image: install-pip-dependencies $(YQ)
	CERTIFIED=${CERTIFIED} DEFAULT_CHANNEL=${BUNDLE_DEFAULT_CHANNEL} CHANNELS=${BUNDLE_CHANNELS} ./scripts/ci/build.py bundle --push ${VERSION} ${APP_VERSION}

build-and-push-catalog-source: install-pip-dependencies $(OPM)
	CERTIFIED=${CERTIFIED} PLATFORM=${PLATFORM} ./scripts/ci/build.py catalog --push ${VERSION} ${APP_VERSION}

build-and-push-catalog-source-manifest: install-pip-dependencies
	CERTIFIED=${CERTIFIED} PLATFORM=${PLATFORM} ./scripts/ci/build.py manifest catalog --push ${VERSION} ${APP_VERSION}

.PHONY: mocks
mocks: $(MOCKERY)
	find . -type f ! -path '*vendor/*' -name 'mock_*' -delete
	$(MOCKERY) -dir=./controllers -all -inpkg

check_mocks: mocks
	# Checking the differences
	@git --no-pager diff --exit-code -- ./helpers/service/mocks \
		$(shell git ls-files | grep 'mock_' | grep -v 'vendor/') && \
		!(git ls-files -o | grep 'mock_' | grep -v 'vendor/') && \
		echo "Mocks up-to-date!"

controller_gen_objects: $(CONTROLLER_GEN)
	$(CONTROLLER_GEN) object paths=./api/v1beta2/runner_types.go

check_controller_gen_objects: controller_gen_objects
	# Checking the differences
	@git --no-pager diff --exit-code -- ./api/v1beta2 \
		$(shell git ls-files | grep 'zz_generated') && \
		!(git ls-files -o | grep 'zz_generated') && \
		echo "controller-gen objects up-to-date!"

.PHONY: community-operator
community-operator: $(YQ)
	mkdir -p community-operator/${VERSION}
	cd bundle && cp -r manifests metadata tests ../bundle.Dockerfile ../community-operator/${VERSION}
	yq e ".metadata.annotations.containerImage = \"${IMG}\"" -i community-operator/$(VERSION)/manifests/gitlab-runner-operator.clusterserviceversion.yaml
	if [[ -z "${PREV_VERSION}" ]]; then \
		yq e "del(.spec.replaces)" -i community-operator/$(VERSION)/manifests/gitlab-runner-operator.clusterserviceversion.yaml; \
	else \
		yq e ".spec.replaces = \"gitlab-runner-operator.v${PREV_VERSION}\"" -i community-operator/$(VERSION)/manifests/gitlab-runner-operator.clusterserviceversion.yaml; \
	fi

	./scripts/process_community_operator_dockerifle.py $(VERSION)

.PHONY: generate_changelog
generate_changelog: export CHANGELOG_RELEASE ?= dev
generate_changelog: $(GITLAB_CHANGELOG)
	# Generating new changelog entries
	@$(GITLAB_CHANGELOG) -project-id 22848448 \
		-release $(CHANGELOG_RELEASE) \
		-starting-point-matcher "v[0-9]*.[0-9]*.[0-9]*" \
		-config-file .gitlab/changelog.yml \
		-changelog-file CHANGELOG.md
